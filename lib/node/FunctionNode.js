var Node = require('./Node');

var specialHandlingFunctions = ['gridRowCounter', 'dataGridColumnTotal'];

function FunctionNode(name, args) {
  if (specialHandlingFunctions.includes(name) && args.length >= 1) {
    this[name] = [ args[0].var ]
  } else if (name === 'extractValueFromGrid' && args.length === 4) {
    this[name] = [ args[0] ]
    this[name].push(args[1].var.split('_collection_').pop())
    this[name].push(args[2].var.split('_collection_').pop())
    this[name].push(args[3])
  } else {
    this[name] = args || [];
  }
}

FunctionNode.prototype = Object.create(Node.prototype);

FunctionNode.prototype.type = 'FunctionNode';

module.exports = FunctionNode;
